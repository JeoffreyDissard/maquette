<?php

namespace MaquetteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Maquette/Default/index.html.twig');
    }
    public function aboutAction()
    {
        return $this->render('@Maquette/Default/about.html.twig');
    }
    public function pricingAction()
    {
        return $this->render('@Maquette/Default/pricing.html.twig');
    }
}
